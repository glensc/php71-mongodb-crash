# php openssl global lock reproducer

`php -m` crashes again, if `curl`, `mongodb` and `openssl` extensions are present

http://lists.pld-linux.org/mailman/pipermail/pld-devel-en/2017-October/025436.html

## building locally

```
# docker build . -t php71-mongodb-crash
```

## obtain pre-built image

```
# docker pull registry.gitlab.com/glensc/php71-mongodb-crash:20180402_1
```

## run gdb

```
# docker run --rm -it php71-mongodb-crash gdb --ex 'set pagination off' --ex run --ex bt --args php71 -m
GNU gdb (GDB) 8.1-1 (PLD Linux)
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-pld-linux".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from php71...Reading symbols from /usr/lib/debug/usr/bin/php71.debug...done.
done.
Starting program: /usr/bin/php71 -m
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib64/libthread_db.so.1".
[PHP Modules]
Core
curl
date
json
libxml
mongodb
openssl
pcre
Reflection
SimpleXML
SPL
standard

[Zend Modules]


Program received signal SIGSEGV, Segmentation fault.
0x00007ffff0163e40 in ?? ()
#0  0x00007ffff0163e40 in ?? ()
#1  0x00007ffff316e8a2 in CRYPTO_THREADID_current (id=0x7fffffffe7c0) at cryptlib.c:500
#2  0x00007ffff322df58 in ERR_remove_thread_state (id=id@entry=0x0) at err.c:1007
#3  0x00007ffff496ccda in Curl_ossl_cleanup () at vtls/openssl.c:991
#4  0x00007ffff4971c47 in Curl_ssl_cleanup () at vtls/vtls.c:177
#5  0x00007ffff4936fce in curl_global_cleanup () at easy.c:329
#6  0x00007ffff4b9a828 in zm_shutdown_curl (type=<optimized out>, module_number=8) at /usr/src/debug/php-7.1.15/ext/curl/interface.c:1419
#7  0x00007ffff7a24157 in module_destructor (module=module@entry=0x667b80) at /usr/src/debug/php-7.1.15/Zend/zend_API.c:2501
#8  0x00007ffff7a1c97c in module_destructor_zval (zv=<optimized out>) at /usr/src/debug/php-7.1.15/Zend/zend.c:633
#9  0x00007ffff7a2f998 in _zend_hash_del_el_ex (prev=<optimized out>, p=<optimized out>, idx=<optimized out>, ht=<optimized out>) at /usr/src/debug/php-7.1.15/Zend/zend_hash.c:997
#10 _zend_hash_del_el (p=0x628070, idx=7, ht=0x7ffff7dd4620 <module_registry>) at /usr/src/debug/php-7.1.15/Zend/zend_hash.c:1020
#11 zend_hash_graceful_reverse_destroy (ht=ht@entry=0x7ffff7dd4620 <module_registry>) at /usr/src/debug/php-7.1.15/Zend/zend_hash.c:1476
#12 0x00007ffff7a2253c in zend_destroy_modules () at /usr/src/debug/php-7.1.15/Zend/zend_API.c:1978
#13 0x00007ffff7a1d421 in zend_shutdown () at /usr/src/debug/php-7.1.15/Zend/zend.c:876
#14 0x00007ffff79ba03b in php_module_shutdown () at /usr/src/debug/php-7.1.15/main/main.c:2445
#15 0x0000000000404d3f in main (argc=2, argv=0x61e3d0) at /usr/src/debug/php-7.1.15/sapi/cli/php_cli.c:1396
(gdb)
```
