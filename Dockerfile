FROM registry.gitlab.com/pld-linux/pld

# force primary mirror
RUN set -x \
	&& sed -i -e 's,ftp://ftp.th.pld-linux.org/dists/th,http://ftp1.pld-linux.org/dists/th,' /etc/poldek/repos.d/*.conf \
	&& poldek -l

RUN set -x \
	&& poldek -n th -n th-debuginfo -u \
		php71-debuginfo curl-debuginfo openssl-debuginfo php71-pecl-mongodb-debuginfo gdb build-essential php71-devel \
		php71-pecl-mongodb php71-curl php71-openssl php71-cli \
	&& poldek --clean-whole

# drop module loader order workaround
RUN cd /etc/php71/conf.d && mv openssl_mongodb.ini mongodb.ini
